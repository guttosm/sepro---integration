package br.com.mastertech.sepro.core.service.impl;

import br.com.mastertech.sepro.core.integration.SeproIntegrationFeign;
import br.com.mastertech.sepro.core.service.ValidadorDocumentoService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ValidadorDocumentoServiceImpl implements ValidadorDocumentoService {

    private SeproIntegrationFeign seproIntegrationFeign;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public ValidadorDocumentoServiceImpl(SeproIntegrationFeign seproIntegrationFeign) {
        this.seproIntegrationFeign = seproIntegrationFeign;
    }

    @Override
    public String validaCNPJ(String numeroCNPJ) {
        return null;
    }
}