package br.com.mastertech.sepro.core.integration;

import br.com.mastertech.sepro.ws.dto.CNPJDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(url = "${ms-sepro.request.mapping.external.sepro}", name = "sepro")
public interface SeproIntegrationFeign {

    @GetMapping(value = "/cnpj/{numeroDocumento}")
    CNPJDTO consultarSituacaoCNPJNNoSepro(@RequestHeader("Authorization") String token, @PathVariable("numeroDocumento") String numeroDocumento);
}