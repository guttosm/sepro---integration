package br.com.mastertech.sepro.core.service;

public interface ValidadorDocumentoService {

    String validaCNPJ(String numeroCNPJ);
}