package br.com.mastertech.sepro;

import br.com.mastertech.sepro.ws.config.FeignConfiguration;
import br.com.mastertech.sepro.ws.config.JacksonConfiguration;
import br.com.mastertech.sepro.ws.config.ModuleConfiguration;
import br.com.mastertech.sepro.ws.config.SwaggerConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        new SpringApplicationBuilder(
                ModuleConfiguration.class,
                Application.class,
                FeignConfiguration.class,
                JacksonConfiguration.class,
                SwaggerConfiguration.class
        ).run(args);
    }
}


