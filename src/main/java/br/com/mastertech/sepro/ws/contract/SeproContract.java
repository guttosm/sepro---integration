package br.com.mastertech.sepro.ws.contract;

import br.com.mastertech.sepro.ws.dto.RespostaPadrao;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface SeproContract {

    @GetMapping(value = "${ms-sepro.request.mapping.internal.cnpj.validaNumero}")
    RespostaPadrao<String> validadarCNPJ(@RequestParam(required = true) String numeroCNPJ);

}
