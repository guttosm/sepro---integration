package br.com.mastertech.sepro.ws.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EnderecoDTO {

    private String bairro;
    private String complemento;
    private String cep;
    private String municipio;
    private String uf;
    private String logradouro;
    private String numero;

}