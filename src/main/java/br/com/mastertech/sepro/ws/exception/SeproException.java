package br.com.mastertech.sepro.ws.exception;

import java.util.Collections;
import java.util.List;

public class SeproException extends RuntimeException {

    private final List<String> reasons;

    public SeproException(String message) {
        super(message);
        reasons = null;
    }

    public SeproException(String message, Throwable cause) {
        super(message, cause);
        reasons = null;
    }

    public SeproException(Throwable cause) {
        super(cause);
        reasons = null;
    }

    public SeproException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        reasons = null;
    }

    public SeproException(String message, List<String> reasons) {
        super(message);
        this.reasons = reasons;
    }

    public SeproException(String message, String reason) {
        super(message);
        this.reasons = Collections.singletonList(reason);
    }
}
