package br.com.mastertech.sepro.ws.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CNPJDTO {

    private String ni;
    private String data_abertura;
    private String nome_empresarial;
    private String nome_fantasia;
    private CodigoDescricaoDTO cnae_principal;
    private CodigoDescricaoDTO natureza_juridica;
    private EnderecoDTO endereco;
    private String situacao_especial;
    private SituacaoCadastralDTO situacao_cadastral;
    private String orgao;
    private String tipo_estabelecimento;
    private String correio_eletronico;
    private String capital_social;
    private String porte;
    private List<TelefoneDTO> telefones;
    private String nome_orgao;
    private String ente_federativo;
    private String data_situacao_especial;


}