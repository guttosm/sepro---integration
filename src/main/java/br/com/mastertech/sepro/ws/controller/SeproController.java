package br.com.mastertech.sepro.ws.controller;

import br.com.mastertech.sepro.core.service.ValidadorDocumentoService;
import br.com.mastertech.sepro.ws.contract.SeproContract;
import br.com.mastertech.sepro.ws.dto.RespostaPadrao;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@Api(value = "Cadastro e valida\u00E7\u00F5es de CNPJ", tags = "CNPJ")
public class SeproController implements SeproContract {

    private ValidadorDocumentoService validadorDocumentoService;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public SeproController(ValidadorDocumentoService validadorDocumentoService) {
        this.validadorDocumentoService = validadorDocumentoService;
    }

    @Override
    public RespostaPadrao<String> validadarCNPJ(@RequestParam(required = false) String numeroCNPJ) {
        logger.debug("-----------> INICIO DA VALIDA\u00C7\u00C3O DO CNPJ");
        RespostaPadrao<String> response = RespostaPadrao.<String>builder()
                .conteudo(validadorDocumentoService.validaCNPJ(numeroCNPJ))
                .valido(Boolean.TRUE)
                .build();
        logger.debug("-----------> FIM DA VALIDA\u00C7\u00C3O DO CNPJ");
        return response;
    }

}
