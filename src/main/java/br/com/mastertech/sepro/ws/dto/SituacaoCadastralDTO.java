package br.com.mastertech.sepro.ws.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SituacaoCadastralDTO {

    private String codigo;
    private String motivo;
    private String data;
}
